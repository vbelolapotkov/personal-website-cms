import React from 'react'
import PropTypes from 'prop-types'
import Image from './common/Image'
import ExternalLink from './common/ExternalLink'

const PostImage = ({ image, caption, className = '' }) => {
  const figcaption = composeImageCaption(caption)
  return (
    <figure className={`post-image ${className}`}>
      <Image image={{ ...image, alt: 'Featured image' }} />
      {figcaption && <figcaption>{figcaption}</figcaption>}
    </figure>
  )
}

PostImage.propTypes = {
  image: Image.propTypes.image,
  caption: PropTypes.string,
  className: PropTypes.string,
}

const composeImageCaption = caption => {
  if (!caption || typeof caption !== 'string') {
    return null
  }

  try {
    const { author, source } = JSON.parse(caption)
    if (!author) {
      return null
    }
    return (
      <>
        {'Photo by '}
        {composeCaptionLink(author)}
        {source && ' on '}
        {source && composeCaptionLink(source)}
      </>
    )
  } catch (err) {
    return caption
  }
}

// eslint-disable-next-line react/prop-types
const composeCaptionLink = ({ name = '', url }) =>
  url ? <ExternalLink href={url}>{name}</ExternalLink> : name

export default PostImage
