---
templateKey: project-page
projectName: Jackal Dungeon Web
description: 'Jackal Dungeon Web is a web version of a board game Jackal: The dungeon.'
thumbnail: /img/jackal_thumb.jpg
tech:
  - Meteor
  - Bootstrap
  - FabricJs
repository:
  isPrivate: false
  linkText: View on Github
  url: 'https://github.com/vbelolapotkov/jackal-dungeon'
publicUrl: 'https://jackal-dungeon-demo.herokuapp.com/'
---
![Jackal Dungeon Web demo](/img/jd_demo.gif "App Demo")

## Project story

Jackal Dungeon Web is a web version of a board game [Jackal: The dungeon](http://www.mosigra.ru/Face/Show/jackal_podzemelie/). The project followed [Munchkin Web](/projects/munchkin-web/) project and had similar goals and prerequisites: have fun with friends and practice my dev skills. Jackal was a very good fit because game mechanics differs significantly from Munckin’s so there were new challenges to solve.

## Key requirements

Here is the list of key requirements

1. It should work in a browser.
2. Game requirements:
   1. It should sync well between players.
   2. It should have a dungeon map composed of tiles by players during the game.
   3. Players should be able to drag and drop tiles to build the map (dungeon map).
   4. Players should be able to rotate tiles.
   5. Players should be able to attach/detach/remove tiles from the map.
   6. Only the player who has taken the tile should have controls over its position. Other players can see where the tile is being moved.
   7. Each player should have its own chip to be moved around the map.
   8. Synced tile and chip moves should be animated.
   9. Coins should be automatically placed into treasure room tiles.
   10. A player should be able to take coin only from the same tile.
   11. A player should be able to initiate a “competition” according to game rules.
   12. Each player should be able to move the dungeon map on the screen locally without affecting other players.
3. A player should be able to join/return the game without an account by using the same nickname and passphrase.
4. There should be several game rooms.

## Research & Design

I was satisfied using the [Meteor](https://meteor.com) framework for Munchkin Web so decided to use it again as it was a very good fit for this project as well. However, ui and the controls in this project were absolutely different. The most interesting and difficult part was an implementation of a map builder, and I considered two different approaches: HTML markup vs canvas. I was already familiar with drag-n-drop features of HTML so I decided to try an alternative way of creating such a game. That’s why I’ve chosen canvas as a foundation of a map builder. 

Native canvas API is not that easy to use directly so I decided to use some canvas library. I’ve searched and checked multiple canvas libraries and have chosen [FabricJs](http://fabricjs.com/) for this project. It has a very nice set of features (object model, a rich event system, animation support, etc) matching project requirements and good documentation.

In this project, the main focus was on the game room, so the lobby page was made pretty simple without any sophisticated design or features. 

The lobby page has a basic intro and a list of game rooms with basic information about players in the room. [Bootstrap](https://getbootstrap.com/) toolkit is used for styling elements of the page.

![Jackal Dungeon Web lobby](/img/jd_lobby.png "Lobby page")

Game room page has several functional areas:

* Navbar with general game controls.
* List of players with their game state (name, chip colour, number of coins, and “competition” attempts left).
* Map area where main interaction takes place. It has the following components:
  * Deck of tiles
  * Map
  * Players chips
  * Two sets of dice (single and double)

![Jackal Dungeon Web game page](/img/jd_game.png "Game page")

[Blaze](https://docs.meteor.com/api/blaze.html) is used as a view engine because it was a primary view engine for Meteor at that time. Map area of the game room is implemented with [FabricJs](http://fabricjs.com/) canvas library.

Routing is implemented with the [iron-router](https://atmospherejs.com/iron/router) package - recommended routing solution at that time.

## Deployment

The demo app is deployed on [Heroku](https://heroku.com). The deployment process is automated via integration with GitHub. The app is deployed for demonstration purposes only and for personal usage. 

## Conclusion

This project allowed me to learn and experiment with HTML canvas and get more experience with amazing framework Meteor. The development of this game supported the course set by Munchkin app: use new technologies to bring fun and common leisure to people distributed around the country.
