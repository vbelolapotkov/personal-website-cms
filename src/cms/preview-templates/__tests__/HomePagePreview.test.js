import React from 'react'
import { Map, List } from 'immutable'
import { render } from 'test-utils'

import HomePagePreview from '../HomePagePreview'

test('renders with empty data', () => {
  const entry = Map({
    data: new Map({})
  })

  const fieldsMetaData = Map({
    projects: List([])
  })
  render(<HomePagePreview entry={entry} fieldsMetaData={fieldsMetaData} />)
})

test('renders with sections', () => {
  const entry = Map({
    data: Map({
      introSection: Map({
        title: 'Test title',
        subtitle: 'Test subtitle',
        moto: 'Test moto'
      }),
      projectsSection: Map({
        title: 'Projects',
        projects: List(['test1', 'badProject', 'test2'])
      }),
      technologySection: Map({
        title: 'test tech',
        subtitle: 'test sub tech',
        principles: Map({
          title: 'Principles',
          list: List([
            Map({
              principle: 'principle 1',
              description: 'principle description 1'
            }),
            Map({
              principle: 'principle 2',
              description: 'principle description 2'
            })
          ])
        }),
        stack: Map({
          title: 'test tech stack',
          techBlocks: List([
            Map({
              techTags: List(['t1', 't2'])
            }),
            Map({
              techTags: List(['t11', 't12'])
            }),
            Map({
              techTags: List(['t21', 't22'])
            })
          ])
        })
      }),
      contactSection: Map({
        title: 'test contact',
        subtitle: 'test subtitle',
        email: 'email@test.local'
      })
    })
  })

  const fieldsMetaData = Map({
    projects: Map({
      test1: Map({
        projectName: 'test1',
        description: 'test1 description',
        thumbnail: { publicUrl: '/img/test1.jpg' },
        tech: List(['t1', 't11', 't21']),
        repository: Map({
          isPrivate: false,
          url: 'https://repo1.test.local',
          linkText: 'view src 1'
        }),
        publicUrl: 'https://public1.test.local'
      }),
      test2: Map({
        projectName: 'test2',
        description: 'test2 description',
        thumbnail: { publicUrl: '/img/test2.jpg' },
        tech: List(['t2', 't12', 't22']),
        repository: Map({
          isPrivate: false,
          url: 'https://repo2.test.local',
          linkText: 'view src 2'
        }),
        publicUrl: 'https://public2.test.local'
      })
    })
  })

  const { container, getByText, queryByText } = render(
    <HomePagePreview entry={entry} fieldsMetaData={fieldsMetaData} />
  )

  expect(getByText('test1')).toBeInTheDocument()
  expect(getByText('test2')).toBeInTheDocument()
  expect(queryByText('badProject')).not.toBeInTheDocument()

  expect(container).toMatchSnapshot()
})

test('renders with empty projects collection', () => {
  const entry = Map({
    data: Map({
      projectsSection: Map({
        title: 'Projects',
        projects: List(['badProject'])
      })
    })
  })

  const fieldsMetaData = Map({})

  const { container, queryByText } = render(
    <HomePagePreview entry={entry} fieldsMetaData={fieldsMetaData} />
  )
  expect(queryByText('badProject')).not.toBeInTheDocument()
  expect(container).toMatchSnapshot()
})
