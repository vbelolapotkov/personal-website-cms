import React, { useState } from 'react'
import { Link } from 'gatsby'
import ExternalLink from './common/ExternalLink'
import { GithubIcon, LinkedinIcon, EnvelopeOutlineIcon } from './common/icons'
import { isBrowser } from '@utils'

const Navbar = () => {
  const [menuIsActive, setMenuIsActive] = useState(false)
  const toggleMenu = () => setMenuIsActive(!menuIsActive)
  const isHomePage = isBrowser() && window.location.pathname === '/'

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <ExternalLink
            className="navbar-item"
            href="https://github.com/vbelolapotkov"
          >
            <GithubIcon />
          </ExternalLink>
          <ExternalLink
            className="navbar-item"
            href="https://www.linkedin.com/in/vasily-belolapotkov"
          >
            <LinkedinIcon />
          </ExternalLink>
          <ExternalLink
            className="navbar-item"
            href="mailto:'Vasily Belolapotkov <belolapotkov.v@gmail.com>'"
          >
            <EnvelopeOutlineIcon />
          </ExternalLink>

          <a
            role="button"
            aria-label="menu"
            aria-expanded="false"
            className={
              menuIsActive ? 'navbar-burger is-active' : 'navbar-burger'
            }
            onClick={toggleMenu}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div className={menuIsActive ? 'navbar-menu is-active' : 'navbar-menu'}>
          <div className="navbar-end">
            <Link
              className={`navbar-item has-text-weight-bold ${
                isHomePage ? 'is-hidden' : ''
              }`}
              to="/"
            >
              {'<Home />'}
            </Link>
            <Link
              className="navbar-item has-text-weight-bold"
              target="portfolio"
              to="/#portfolio"
            >
              {'<Portfolio />'}
            </Link>
            <Link
              className="navbar-item has-text-weight-bold"
              target="tech"
              to="/#tech"
            >
              {'<TechStack />'}
            </Link>
            <Link className="navbar-item has-text-weight-bold" to="/blog">
              {'<Blog />'}
            </Link>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
