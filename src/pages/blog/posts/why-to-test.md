---
templateKey: blog-post-page
title: 'Why to test software'
posted: '2020-06-09'
tags: 'testing, automation, productivity'
featuredImage: '/img/why-to-test.jpg'
featuredImageCaption: '{"author":{"name":"National Cancer Institute","url":"https://unsplash.com/@nci?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"},"source":{"name":"Unsplash","url":"https://unsplash.com/s/photos/benefits-of-testing?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"}}'
abstract: "Read the post to find out the benefits of testing and how they can make developer life better."

---

> Why do we test software? Most people would say: “So we know it works.”
>
> -- From [this blog](https://www.techwell.com/techwell-insights/2017/09/why-do-we-test-software)

I like this answer because it's short, simple and matches my point of view 🙂 I **do** want to know the software I create works!

It takes time and effort to master testing skills and learn necessary tools. Many developers don't write tests and they have their [own excuses](https://hackernoon.com/common-excuses-why-developers-dont-test-their-software-908a465e122c) for it. And there's a bunch of positions between these two poles: haven't decided yet, just heard something, once tried it, but it didn't go smoothly, etc. Anyway, before investing your time and effort IMO it would be good to know why and how testing can make your life better. Further you'll find the benefits I see and utilize with tests.

## Mistakes

[Every developer makes mistakes](https://tosbourn.com/every-developer-makes-mistakes-all-the-time/). And it's not because they are bad or inexperienced, but because they are humans and humans makes mistakes.

Moreover modern software is built incrementally by adding smaller changes and combining many independent units. And the more pieces the software has the higher is the probability to introduce a mistake with new changes.

**=> Tests are our validators for the software. Creating more tests validates more scenarios hence reduces amount of mistakes left unnoticed.**

## Product quality

Product quality is a multidimensional topic. While stability is only one of the quality aspects it's hard to overestimate its influence: it's very unlikely you would call a product a good quality one if it constantly breaks while working or after each update. And vice versa we can say higher quality products are more stable and have less mistakes.

**=> Tests reduce amount of mistakes and raise product quality!**

## Confidence

**Developers are humans**, I know I've already told this but this time it means each developer is a living person! And I doubt many developers are looking for stress and sleepless nights at their work due to buggy software or poorly tested product releases, constant fights with fires and so on. It might sound a bit dramatic and I haven't been in such situations but I've read about them in the books (e.g. [DevOps Handbook](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002)) and suppose it's real.

Tests validate that software behaves the way it is expected. Running tests gives a confidence that expectations are valid, confidence reduces stress and less stress leads to a healthier and happier life. One really important thing here is *the tests are useful as long as expectations described in the tests match the way the software should behave from the consumer point of view*. Poorly written tests validate expectations meaningless to how software is consumed.

**=> Running tests gives a confidence that expectations are valid, confidence reduces stress and less stress leads to a healthier and happier life.**

## Architecture

Software is not a building, its architecture changes and evolves during the lifetime. So it's quite common to add functionality to existing modules, split large modules into smaller ones, break dependencies and rearrange the things. All those changes can potentially break some existing functionality.

**=> Tests allow us to catch those regressions before they reach a production, making refactoring and architecture improvements more stable and reliable.**

There is another way how tests can improve architecture if you run them before ([TDD](https://en.wikipedia.org/wiki/Test-driven_development) way) or right after the new code.

**=> Writing tests forces developer to think about how to put the code under test and how is it going to be consumed leading to better code structure and modularity.**

## Documentation

Writing docs is not the most pleasant work for developers and they are [usually not so good at it](https://dzone.com/articles/why-developers-write-horrible-documentation-and-ho). It's quite hard to maintain the docs up to date especially if they live far from the source code. Even if docs are written withing the code it's quite easy to forget to update them once the code is changed just by *mistake*. On the other hand if tests are out of sync with code they fail and signal you that changes are required.

**=> Tests are a good source of developer documentation with examples of how to use modules and how edge cases are handled by the software.**

## Time & Money

Everything above is correct for tests in general but why automated tests? Because the more features the software has the more cases have to be tested.
And the more scenarios you test the more time, and mental efforts it requires from tester. Testers are humans as well and we're back to **mistakes** ☝️.
Scaling manual testing efforts is way more expensive as compared to scaling computational resources for automated testing.

On the other hand machines are way faster and better than humans at doing mundane and repeating tasks. And most of tests are like this. Automated tests can be run super fast and very often providing all benefits mentioned above: less mistakes, better product, happier team, happier customers.

**=> Automated testing allows to spend more time and money on innovations and improvements by saving costs for maintenance and bug fixing.**

## Conclusion

Real software might be complex and might have many various requirements that's why there are [many different ways](https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing) to check the software works as expected. Automated testing is only one of the ways and it's not a silver bullet. It has its own costs and tradeoffs and if done wrong can even harm the project.

However, mindful, disciplined approach, patience and practice pay back allowing you to get all the benefits mentioned above making it extremely hard to switch back to creating software without tests.
