import React from 'react'
import Layout from '../components/Layout'

const NotFoundPage = () => (
  <Layout>
    <div className="section container">
      <h1>PAGE NOT FOUND</h1>
      <p>You just hit a URL that doesn&apos;t exist...</p>
    </div>
  </Layout>
)

export default NotFoundPage
