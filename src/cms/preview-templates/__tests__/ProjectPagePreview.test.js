import React from 'react'
import { Map, List } from 'immutable'
import { render } from 'test-utils'
import ProjectPagePreview from '../ProjectPagePreview'

const entry = Map({
  data: Map({
    projectName: 'project name',
    description: 'project description',
    tech: List(['js', 'html']),
    repository: Map({
      isPrivate: true
    }),
    publicUrl: 'https://example.com'
  })
})
const widgetFor = jest.fn(() => <p>mocked widget</p>)

test('it should render preview component', () => {
  const { container } = render(
    <ProjectPagePreview entry={entry} widgetFor={widgetFor} />
  )

  expect(container).toMatchSnapshot()
})
