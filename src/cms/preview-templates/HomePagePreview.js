import React from 'react'
import PropTypes from 'prop-types'
import { HomePageTemplate } from '../../templates/home-page'

const HomePagePreview = ({ entry, fieldsMetaData }) => {
  const sections = entry.getIn(['data']).toJS()

  if (
    sections.projectsSection &&
    sections.projectsSection.projects &&
    sections.projectsSection.projects.length > 0
  ) {
    const projectsMeta = fieldsMetaData.getIn(['projects'])
      ? fieldsMetaData.getIn(['projects']).toJS()
      : {}

    sections.projectsSection.projects = sections.projectsSection.projects
      .map(projectName => projectsMeta[projectName])
      .filter(project => !!project)
  }

  return <HomePageTemplate {...sections} />
}

HomePagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func
  }),
  widgetFor: PropTypes.func,
  fieldsMetaData: PropTypes.shape({
    getIn: PropTypes.func
  })
}

export default HomePagePreview
