import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import Layout from '../components/Layout'
import Head from '../components/Head'
import Post from '../components/Post'
import PostBody from '../components/PostBody'
import PostImage from '../components/PostImage'

const BlogPostPageTemplate = ({ location, data }) => {
  const {
    site: { siteMetadata: meta },
    markdownRemark: { id, html, frontmatter, timeToRead },
  } = data

  const post = {
    id,
    url: location.href,
    title: frontmatter.title,
    posted: frontmatter.posted,
    timeToRead,
    tags:
      frontmatter.tags && frontmatter.tags.length
        ? frontmatter.tags.split(',').map(tag => tag.trim())
        : [],
  }

  return (
    <Layout>
      <Head meta={meta} />
      <div className="container content">
        <Post post={post}>
          <PostImage
            image={frontmatter.featuredImage}
            caption={frontmatter.featuredImageCaption}
          />
          <PostBody content={html} isRawHTML />
        </Post>
      </div>
    </Layout>
  )
}

BlogPostPageTemplate.propTypes = {
  location: PropTypes.shape({ href: PropTypes.string }),
  data: PropTypes.shape({
    site: PropTypes.shape({ siteMetadata: Layout.propTypes.meta }),
    markdownRemark: PropTypes.shape({
      id: PropTypes.string,
      html: PropTypes.string,
      timeToRead: PropTypes.number,
      frontmatter: PropTypes.object,
    }),
  }),
}

export const pageQuery = graphql`
  query PostByID($id: String!) {
    site {
      siteMetadata {
        title
        description
      }
    }
    markdownRemark(id: { eq: $id }) {
      id
      timeToRead
      html
      frontmatter {
        title
        tags
        posted(fromNow: true)
        featuredImageCaption
        featuredImage {
          publicURL
          childImageSharp {
            fluid(maxWidth: 600) {
              ...GatsbyImageSharpFluid_tracedSVG
            }
          }
        }
      }
    }
  }
`

export default BlogPostPageTemplate
