# [vbelolapotkov.com](https://vbelolapotkov.com)

Personal web site built with [Gatsby](https://www.gatsbyjs.org/) and [Netlify CMS](https://www.netlifycms.org).

Bootstrapped with [Gatsby + Netlify CMS Starter](https://github.com/netlify-templates/gatsby-starter-netlify-cms)

Deployed with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
