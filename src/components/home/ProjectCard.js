import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '../common'
import ExternalLink from '../common/ExternalLink'
import Image from '../common/Image'

const ProjectCard = ({ project }) => (
  <div className="card" style={{ height: '100%' }}>
    <Link to={project.path} className="card-image">
      <Image image={{ ...project.thumbnail, alt: 'Thumbnail image' }} />
    </Link>
    <div className="card-content">
      <div className="title">
        <Link to={project.path}>{project.projectName}</Link>
      </div>
      <div className="content">
        <p style={{ minHeight: '80px' }}>{project.description}</p>
      </div>

      <div className="columns">
        <div className="column">
          <div className="tags">
            {project.tech.map(tag => (
              <span key={`${tag}_tag`} className="tag is-info gtm-tech-tag">
                {tag}
              </span>
            ))}
          </div>
        </div>
        <div className="column is-narrow">
          <div>
            {project.repository.isPrivate ? (
              <span className="tag is-light">
                {project.repository.linkText}
              </span>
            ) : (
              <ExternalLink
                href={project.repository.url}
                className="tag is-link"
              >
                {project.repository.linkText}
              </ExternalLink>
            )}
          </div>
        </div>
      </div>
    </div>
  </div>
)

ProjectCard.propTypes = {
  project: PropTypes.shape({
    projectName: PropTypes.string.isRequired,
    path: PropTypes.string,
    thumbnail: Image.propTypes.image,
    publicUrl: PropTypes.string,
    description: PropTypes.string,
    tech: PropTypes.arrayOf(PropTypes.string),
    repository: PropTypes.shape({
      isPrivate: PropTypes.bool,
      linkText: PropTypes.string,
      url: PropTypes.string,
    }),
  }).isRequired,
}

export default ProjectCard
