---
templateKey: project-page
projectName: Calculator app
description: A simple calculator app supporting a keyboard input.
thumbnail: /img/calc_thumb.png
tech:
  - React
  - Mocha
  - Heroku
repository:
  linkText: View on Github
  url: 'https://github.com/vbelolapotkov/fcc-calc'
publicUrl: 'https://fcc-calc.herokuapp.com/'
---
![Calculator app demo](/img/calc_demo.gif "Calculator app")

The project was implemented as part of [freeCodeCamp](https://www.freecodecamp.org/) curriculum. It is a pure React app implemented without other dependencies. Apart from mouse input, the calculator supports keyboard input as well. The app is deployed on [Heroku](https://heroku.com).
