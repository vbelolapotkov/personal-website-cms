---
templateKey: project-page
projectName: SVG charts
description: >-
  The app was developed as part of Telegram coding contest without usage of 3rd
  party libraries.
thumbnail: /img/tg_chart_contest_thumb.png
tech:
  - TypeScript
  - SVG
  - Webpack
repository:
  isPrivate: false
  linkText: View in GitLab
  url: 'https://gitlab.com/vbelolapotkov/telegram-chart-contest'
publicUrl: 'https://tg-chart-contest.vbelolapotkov.com/'
---
![Simple charts app](/img/tg_chart_contest.png)

The app is a JavaScript app for showing simple charts data based on provided input data. The app was developed as part of Telegram coding contest without usage of 3rd party libraries.

**Note:** Source code of the app will be published soon, right after contest results announced.

## Contest challenge

Note: you may not use specialized charting libraries. All the code you submit must be written by you from scratch.

The criteria we’ll be using to define the winner are speed, efficiency and the size of the app.

The app should show 4 charts on one screen, based on the input data we will provide.

## Tech Stack

* Language: [TypeScript](https://www.typescriptlang.org/). 
* UI: HTML, SVG, [SASS](https://sass-lang.com/).
* Deployment: [Webpack](https://webpack.js.org/) + [GitLab pages](https://about.gitlab.com/product/pages/).
