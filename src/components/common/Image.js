import React from 'react'
import PropTypes from 'prop-types'
import Img from 'gatsby-image'

const Image = ({ image = {} }) =>
  image.childImageSharp ? (
    <Img
      className="image"
      fluid={image.childImageSharp.fluid}
      alt={image.alt}
    />
  ) : (
    <img className="image" src={image.publicURL} alt="Thumbnail image" />
  )

Image.propTypes = {
  image: PropTypes.shape({
    publicURL: PropTypes.string,
    childImageSharp: PropTypes.shape({
      fluid: Img.propTypes.fluid,
    }),
    alt: PropTypes.string,
  }),
}

export default Image
