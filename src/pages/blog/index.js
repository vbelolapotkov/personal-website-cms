import React from 'react'
import PropTypes from 'prop-types'
import { graphql, Link } from 'gatsby'
import Layout from '../../components/Layout'
import PostImage from '../../components/PostImage'

// TODO: 1. Add tags cloud/filter.

const ABSTRACT_LENGTH_LIMIT = 200

const BlogPage = ({ data }) => {
  return (
    <Layout meta={data.site.siteMetadata}>
      <div className="post-container">
        <h1 className="title">Blog posts</h1>
        <div className="posts-list">
          {data.allMarkdownRemark.edges.map(({ node }) => (
            <article key={node.id} className="post">
              <div className="columns">
                <div className="column is-5">
                  <PostImage image={node.frontmatter.featuredImage} />
                </div>
                <div className="column">
                  <h2 className="title">
                    <Link to={node.fields.slug}>{node.frontmatter.title}</Link>
                  </h2>
                  <div className="meta has-text-grey">
                    {node.frontmatter.posted}
                    {node.timeToRead &&
                      node.timeToRead > 1 &&
                      ` | ${node.timeToRead} mins`}
                    {node.frontmatter.tags && [' | ', node.frontmatter.tags]}
                  </div>
                  {node.frontmatter.abstract && (
                    <div className="abstract">
                      {node.frontmatter.abstract.slice(
                        0,
                        ABSTRACT_LENGTH_LIMIT
                      )}
                      {node.frontmatter.abstract.length >
                        ABSTRACT_LENGTH_LIMIT && ' ...'}
                    </div>
                  )}
                </div>
              </div>
            </article>
          ))}
        </div>
      </div>
    </Layout>
  )
}

BlogPage.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({ siteMetadata: Layout.propTypes.meta }),
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.arrayOf(PropTypes.object),
    }),
  }),
}

export const pageQuery = graphql`
  query BlogPageQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
    allMarkdownRemark(
      sort: { fields: frontmatter___posted, order: DESC }
      filter: { frontmatter: { templateKey: { eq: "blog-post-page" } } }
    ) {
      edges {
        node {
          id
          timeToRead
          fields {
            slug
          }
          frontmatter {
            title
            posted(fromNow: true)
            tags
            abstract
            featuredImage {
              publicURL
              childImageSharp {
                fluid(maxWidth: 600) {
                  ...GatsbyImageSharpFluid_tracedSVG
                }
              }
            }
          }
        }
      }
    }
  }
`

export default BlogPage
