---
templateKey: project-page
projectName: Munchkin Web
description: Munchkin Web is a web version of a world-famous board game Munchkin.
thumbnail: /img/munchkin_web_thumb.jpg
tech:
  - Meteor
  - Blaze
  - Heroku
repository:
  isPrivate: false
  linkText: View on Github
  url: 'https://github.com/vbelolapotkov/munchkin-web'
publicUrl: 'https://munchkin-web-demo.herokuapp.com/'
---
## Project story

Munchkin Web is my first web project and where it all started in 2014. I had started learning web development at that time as a hobby and had already read several online books about the HTML, CSS and JS. However, I wanted to make something real and useful, so I had chosen Munchkin because of several reasons. Firstly, I had a lot of fun playing this board game with my friends, but as I moved to another city I hadn’t the opportunity anymore. Secondly, there was an app which could be installed to play online, but it was slow, had synchronisation problems and quite buggy on a mac. 

## Key requirements

So the project was chosen and the basic requirements were as follows:

1. It should work in a browser.
2. Game requirements:
   1. It should sync very well across multiple players.
   2. Users should be able to drag and drop cards around the board.
   3. There should be a deck of cards (actually two decks by game rules).
   4. Some cards should be shown to all participants and some should be visible only to the owner (by game rules).
   5. There should be an option to exchange cards privately between players without exposing cards to the rest.
   6. There should be a common log of actions.
3. It should have basic authentication (username/password).
4. It should be possible to run multiple games in parallel (multiple game rooms).
5. Before the game starts, players should be able to choose a set of supplements.
6. The app should have basic info about the game (description, rules and hints for web users).

## Research & Design

I was familiar with the request/response nature of the web which wasn’t a perfect fit for real-time sync. There was an obvious option with constant polling but I hoped there is a better solution, so I decided to make a research of available solutions. This is how I found out about web sockets and finally about [Meteor](https://meteor.com) framework. Meteor looked like a perfect fit with its reactivity at the core, easy learning curve and ability to write both client and server side in JavaScript. None of the frameworks offered this set of features out of the box:

* Reactive updates from the server to a client
* Client-side rendering
* Accounts system
* A build tool
* Free and easy deployment (at that time)
* Great developer experience with an auto restart of the app on code change
* Very good documentation and community packages

As a first step I’ve made a list of pages (routes) that have to be in the app:

* “Home” page with a list of available game rooms.
* “About” page with info about the game itself in case someone has forgotten or visited the website accidentally.
* “Rules” page just to check the rules in the process of the heat discussion or fight during the game.
* “How-to-play” page with hints and basic info about how to use the web version of the game.
* “Game room” page where the main action takes place - the hardest and the most sophisticated part of the app.

Pages “About”, “Rules” and “How-to-play” are super simple and straightforward as they contain only static content rendered under layout template and are out of the interest.

“Home” page has a dynamic list of existing game rooms and a form to create a new game. Unauthorized user can’t enter any game room or create a new one. No one but the game owner can enter the game room if it’s locked from inside.

![Munchkin Web Lobby](/img/munchkin_lobby.png "Home page view")

“Game room” page has several areas:

* Public areas of each player where items applied to the player are shown. Anyone should be able to check this area during the game.
* The personal area with all player stats (level, power, gender, etc.) and cards you hold privately.
* Table area with decks, drops and where the main action is happening.
* Game controls area with buttons and useful actions like lock/unlock the room, shuffle a deck, roll the dice etc.
* Card preview area which allows seeing the card content on a bigger scale to read properly card meaning which might be super important during the game.

![Munchkin Web game room](/img/munchkin_demo.gif "Game room view")

[Blaze](https://docs.meteor.com/api/blaze.html) is used as a view engine because it was the only supported view engine at that time and it was quite easy to use.

Routing is implemented with the [iron-router](https://atmospherejs.com/iron/router) package - recommended routing solution at that time.

Drag and drop features were implemented with native browser events and data context.

As an experiment, the app was implemented with package only structure offered in this [article](https://www.manuel-schoebel.com/blog/meteorjs-package-only-app-structure-with-mediator-pattern) which offered better granularity and control of modules load order.

## Deployment

Another amazing benefit of Meteor was free deployment under meteor.com subdomain with a single command. As I didn’t have any plans of running the app for a large number of users with significant loads, this option was perfect for a start. However, later this option was disabled so I moved the app to [Heroku](https://heroku.com) which was also free for my case.

## Conclusion

The project allowed me to learn and understand a lot:

1. Key concepts of meteor framework (collections, publications and subscriptions, accounts management, routing, client-side rendering, packages).
2. Native drag and drop in the browser.
3. Experiment with the source code structure.
4. Two deployment options.

However, the most important thing behind this project for me was the possibility to bring real value for my friends and myself - the joy of playing our favourite game and feeling the connection regardless of the distance between us.
