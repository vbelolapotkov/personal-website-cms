---
templateKey: blog-post-page
title: 'Bring structure to chaos with Pomodoro technique'
posted: '2020-02-14'
tags: 'pomodoro, productivity, time-management'
featuredImage: '/img/alarm-clock-by-tristan-gassert-unsplash.jpg'
featuredImageCaption: '{"author":{"name":"Tristan Gassert","url":"https://unsplash.com/@imtilt?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"},"source":{"name":"Unsplash","url":"https://unsplash.com/s/photos/pomodoro-timers?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"}}'
abstract: 'Pomodoro is a powerfull time-management technique. Read the post to find out how I use it during the day.'
---

> There is no such a thing as time management because you can't control time. However, you have ability to choose how you use your time.

I've come across this idea ☝️ several times for the past couple of months. The wording was different but the idea remains the same. Well, it sounds simple as many great ideas but how do you apply it to your daily life? How do you leverage this knowledge when your day is packed with lots of different activities, communications and interruptions?

**TLDR;** I use [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) to split my day into focus slots. If you want to find out more on how I use it keep reading.

## Focus, focus, focus

As it was said many times, [context switching kills productivity](https://blog.trello.com/why-context-switching-ruins-productivity) so staying focused is a solution to the problem. And for developers who often work on complex problems it is very important to be in a [flow](https://www.goodreads.com/book/show/66354.Flow) state with high focus and concentration. To reach that state you need to eliminate as many distractions as possible and it's not that hard to achieve if you work remotely from the home office like I do. But there is another trap in the full focus mode - it is so easy to spend too many time focused on developing software that I can completely forget or miss other things I wanted to do. And this is where Pomodoro technique come to the rescue.

## Managing your day with pomodoros

Okay, I want to be focused for some time but not too long just to keep other things on track. This is exactly what Pomodoro is about:

<!-- wp:list {"ordered":true} -->
1. Pick a task and set a timer (25 minutes is recommended). One slot is called Pomodoro.
2. Work on the task without switching and distractions.
3. Take a break. Recommendation is 5 minutes after each Pomodoro and 15 minutes after each fourth Pomodoro.
4. Go to step 1.


Idea is super simple. This allows you to structure your day into finite amount of focus slots and **you have a choice** which activities you fit into the slots. On the other hand, Pomodoro just gives you a framework routine and it's up to you how to use it. Recommendations above are good enough to start with but they may be not ideal for a particular daily schedule or activities. So some adjustments might be required and the good thing is nobody forbids doing so. Believe me, there is enough space for experiments and here are mine Pomodoro hacks:

<!-- wp:list -->
* I spend at least one Pomodoro at the beginning of the day for planning: catching up with updates (emails, chats, etc.), creating todo list, prioritising and "distributing" tasks into slots.
* I experiment with the size of the slot for different activities. E.g. 55 min slot for development tasks with 5 min break works better for me.
* Use breaks more effectively:
  * Quickly check chats during some of the breaks and update todo list.
  * [Make STOPs](/blog/posts/make-stops) during the breaks to check how I am progressing during the day.
  * Do real breaks, move around, take a glass of water, tea, etc.
* Combine multiple small tasks into a single slot. It's just more efficient when you're focused on handling multiple small tasks altogether.
* Have some free slots without any tasks scheduled to fit in emergency activities. But I have a pool of some low priority tasks in case there are no urgent tasks.
* Allocate some slots even for such things as random search and non-work related activities.
* Allocate slots only for the type of activity rather than precise task. E.g. when planning I don't try to guess how much time a specific task will take I just allocate some slots for development, code reviews, communicating in chats/emails, reading posts, etc.

## Tools

At the moment I use PomoDoneApp for timers both on Mac and iOS, it's free for my use case and has many configuration options which is nice for experiments. I use [Todoist](https://todoist.com) for task management and there is an integration available with PomoDoneApp but I'm not using it because of the reasons mentioned above.

Before switching to PomoDone I used [Be Focused](https://apps.apple.com/ru/app/be-focused-focus-timer/id973130201) for Mac and iOS. There is a free version with annoying ads and without synchronisation between Mac and iOS. I decided not to use it because I constantly missed the timer on the phone because of the vibration only and I had to buy two apps (Desktop and mobile) to make synchronisation work.

## Final thoughts

Pomodoro technique helps me to set time boundaries between different activities I schedule for the day and stay focused while working on each.

I have to say it wasn't that easy to apply the Pomodoro technique to my daily routine. Sometimes I still forget to enable the timer or ignore the end of slot signal when I need some more minutes to finish the task but now I definitely have way more control and structure in my daily chaos.
