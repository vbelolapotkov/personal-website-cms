import React from 'react'
import { HeartOutlineIcon } from './common/icons'

const FooterLink = ({ children, ...props }) => (
  <a
    target="_blank"
    rel="noopener noreferrer"
    className="has-text-light"
    {...props}
  >
    <b>{children}</b>
  </a>
)

const Footer = () => (
  <footer className="footer has-text-light">
    <div className="content has-text-centered is-small">
      <p>
        Handcrafted with <HeartOutlineIcon className="is-small" /> by Vasily
        Belolapotkov. Copyright &copy; 2018.
      </p>
      <p>
        Made with <FooterLink href="https://bulma.io">Bulma</FooterLink>
        {', '}
        <FooterLink href="https://gatsbyjs.org">Gatsby</FooterLink>
        {' and '}
        <FooterLink href="https://netlifycms.org">NetlifyCMS</FooterLink>.
      </p>

      <p>
        Hosted on{' '}
        <FooterLink href="https://www.gitlab.com/">GitLab</FooterLink>. Source
        available on{' '}
        <FooterLink href="https://gitlab.com/vbelolapotkov/personal-website-cms">
          GitLab
        </FooterLink>
        .
      </p>
    </div>
  </footer>
)

export default Footer
