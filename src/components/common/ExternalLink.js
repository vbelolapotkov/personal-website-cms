import React from 'react'
import PropTypes from 'prop-types'

const ExternalLink = ({ children, ...props }) => (
  <a target="_blank external" rel="noopener noreferrer" {...props}>
    {children}
  </a>
)

ExternalLink.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

export default ExternalLink
