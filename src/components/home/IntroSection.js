import React from 'react'
import PropTypes from 'prop-types'

const IntroSection = ({ title, subtitle, moto }) => (
  <section className="hero is-primary is-medium is-bold is-large">
    <div className="hero-body has-text-centered">
      <div className="container">
        <h1 className="title is-size-1 has-text-weight-bold">{title || 'no-title'}</h1>
        <h2 className="subtitle">{subtitle || 'no-subtitle'}</h2>
        <p className="subtitle is-size-4">
          {moto || 'no-moto'}
        </p>
      </div>
    </div>
  </section>
)

IntroSection.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  moto: PropTypes.string,
}

IntroSection.defaultProps = {
  title: 'Full-stack developer',
  subtitle: 'Vasily Belolapotkov',
  moto: 'I enjoy creating solutions'
}

export default IntroSection
