# Security certificate generation

Steps below are taken form GitLab Pages [docs](https://docs.gitlab.com/ee/user/project/pages/lets_encrypt_for_gitlab_pages.html).

## Prerequisites

1. you're in the project dir: `cd path/to/project/dir`
2. certbot installed: `brew install certbot`

## Steps

1. Request certificate with command:

`sudo certbot certonly -a manual -d vbelolapotkov.com -d www.vbelolapotkov.com --email belolapotkov.v@gmail.com`

2. Create acme-challenge files in `static/.well-known/acme-challenge/<challengeKey>/index.html` based on output generated from previous command. **IMPORTANT:** Do not press enter once challenge details are displayed.

3. Push files into the repository and wait untill changes deployed. Make sure acme-challenge urls can be resolved with appropriate keys.

4. Press enter in acme-challenge window to issue new certs. **Note:** verification may fail at this step because of the timeout. In that case reissue the command at step 1 again and skip steps 2 and 3 as the same challenge url and key should be generated.

5. Update public cert and private key on Gitlab Pages in **Settings > Pages > Details > Edit**. Use following commands in terminal to copy certs content into buffer:

`sudo cat /etc/letsencrypt/live/vbelolapotkov.com/fullchain.pem | pbcopy`

`sudo cat /etc/letsencrypt/live/vbelolapotkov.com/privkey.pem | pbcopy`
