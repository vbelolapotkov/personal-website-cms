---
templateKey: project-page
projectName: Human-Connection
description: >-
  Human-Connection is a non-profit social knowledge and action network in order
  to meet the challenges of our time together, to preserve human dignity and to
  create a future worth living for all people and future generations.
thumbnail: /img/humann_connection_thumb.png
tech:
  - Vue
  - Nuxt
  - GraphQL (Apollo)
  - Neo4j
repository:
  linkText: View src code
  url: >-
    https://github.com/Human-Connection/Human-Connection/commits?author=vbelolapotkov
publicUrl: 'https://alpha.human-connection.org/'
---

![human-connection login screen](/img/humann_connection.png)

[Humman-Connection ](https://human-connection.org/en/) is a nice project of a non-profit social network with great mission and focus on people, nature and environment. I've joined the project in September 2019 as a volunteer developer to support very nice team behind it and learn new technologies. Indeed the tech stack of Humman-Connection app is modern and very interesting.

## Tech stack

- Language: JavaScript.
- UI: [Vue.js](https://vuejs.org/)
- Backend: [Nuxt.js](https://nuxtjs.org/), [Neo4j](https://neo4j.com)
- Data: [GraphQL](https://graphql.org) with [Apollo](https://www.apollographql.com)
