---
templateKey: blog-post-page
title: 'Make STOPs to steer your life with open eyes'
posted: '2020-03-26'
tags: 'productivity, stops, time-management'
featuredImage: '/img/stop-sign-by-john-matychuk-unsplash.jpg'
featuredImageCaption: '{"author":{"name":"John Matychuk","url":"https://unsplash.com/@john_matychuk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"},"source":{"name":"Unsplash","url":"https://unsplash.com/s/photos/stop?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"}}'
abstract: 'One can reach the goal faster by taking the shortest path. Make STOPs to stay on track and follow your own plan.'
---

Steering with closed eyes is not the best way to get to the destination. And I believe each of us is a driver with a “steering wheel” in the hands - the steering wheel of life. We have plans and we’re taking actions (or inactions) many times a day the same way as a driver has a route to the destination and choosing when to accelerate, stop and change the route. Ask yourself would you be happy if your taxi driver gets off the route, misses a turn or goes the wrong way? Probably not. While it’s relatively easy to look around and follow the route while driving the car it might be challenging to do the same with your daily life.

**TLDR;** I’ve read about the tool called STOPs in the book [The Inner Game of Work](https://theinnergame.com/inner-game-books/the-inner-game-of-work/) by Timothy Gallwey. I use STOPs to align with my plans and take actions in a timely manner. More details 👇.

## Zooming in and out

In Google Maps you can zoom in and out to see more or fewer details on the streets and roads. Depending on how far you go you might need to zoom out to fit the whole route. However, you should zoom in to get detailed directions to the destination while driving. Moreover, if the route is unfamiliar Google Maps can make life easier by tracking the position, notifying on required actions and suggesting alternative routes.

The same way any higher-level goals and plans can be split into smaller tasks and milestones. The same way precise steps need to be taken to get the result. However, following own plan is hard because there is no such Google Maps for it. You should manually zoom in and out to check your current position on the route and correct the directions to reach the destination.


## Execution inertia is your enemy

Besides our plans and goals, so many things break into the daily routine from different sources like family, coworkers, communities, friends, etc. We are going through hundreds of messages, calls, posts, tweets, meetings and so on. We’re creating task lists to keep things organized and under control.

Even if you apply task and time management tools it’s so easy to get into a trap of unconsciously executing one task after another without realizing the importance of the task or how is it related to the higher-level goals. This is what is called execution inertia. It keeps you driving full speed in the flow of daily tasks without giving a chance to check the directions on the map.

## STOPs to the rescue

I’ve read about the tool called STOPs in the book [The Inner Game of Work](https://theinnergame.com/inner-game-books/the-inner-game-of-work/) by Timothy Gallwey.

STOP is an abbreviation of four steps:

1. Step back.
2. Think.
3. Organize your thoughts.
4. Proceed.

**Step back.** It is a crucial element of the tool. First of all, it requires you to take a pause and step out of the flow of tasks. Then you have to zoom out to create a different perspective on the current situation.

**Think.** Once zoomed out you can better observe the current position and direction, analyze it and compare with the desired route. Just draft some thoughts and ideas at this step.

**Organize your thoughts.** While the previous step can bring a lot of insights and interesting ideas, this step is required to update the existing plan or make a new one.


**Proceed.** You’ve already done a great job at previous steps! It’s time to get back to the refreshed tasks flow 😉


## Different flavours of STOPs

All STOPs shouldn’t be the same. You have a lot of freedom regarding when and how many STOPs you wish to make. They can be done during the day (e.g. start, middle, end of the day), once a week, month, quarter, etc. The duration can vary with the frequency of the STOP and depending on how far you zoom out when stepping back.

At the moment I have daily and monthly STOPs. I try to check the monthly plan a couple of times a week just to keep it in mind during the daily STOPs. And I check the quarterly plan during the monthly stops.

In my opinion, it’s not that important how many STOPs you have. The key point is to get most of them for yourself.
