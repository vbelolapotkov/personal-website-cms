import React from 'react'
import PropTypes from 'prop-types'
import ProjectCard from './ProjectCard'

const ProjectsSection = ({ title, projects }) => (
  <section className="section is-medium" id="portfolio">
    <div className="container">
      <div className="content">
        <h2 className="has-text-weight-bold is-size-2">{title}</h2>
        <div className="columns is-multiline">
          {projects.map((project, index) => (
            <div
              key={`pr_${index}`}
              className="column is-half-tablet is-one-third-desktop"
            >
              <ProjectCard project={project} />
            </div>
          ))}
        </div>
      </div>
    </div>
  </section>
)

ProjectsSection.propTypes = {
  title: PropTypes.string,
  projects: PropTypes.arrayOf(ProjectCard.propTypes.project),
}

ProjectsSection.defaultProps = {
  title: 'Projects',
  projects: [],
}

export default ProjectsSection
