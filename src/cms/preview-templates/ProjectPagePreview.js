import React from 'react'
import PropTypes from 'prop-types'
import { ProjectPageTemplate } from '../../templates/project-page'
import PostBody from '../../components/PostBody'

const ProjectPagePreview = ({ entry, widgetFor }) => {
  const project = entry.getIn(['data']).toJS()

  return (
    <ProjectPageTemplate
      content={() => <PostBody content={widgetFor('body')} />}
      project={project}
    />
  )
}

ProjectPagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func
  }),
  widgetFor: PropTypes.func
}

export default ProjectPagePreview
