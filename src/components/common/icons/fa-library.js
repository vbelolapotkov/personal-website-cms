import { library } from '@fortawesome/fontawesome-svg-core'
import { faGithub, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import {
  faGlobe as fasGlobe,
  faTags as fasTags,
  faChevronUp as fasChevronUp,
} from '@fortawesome/free-solid-svg-icons'
import {
  faEnvelope as farEnvelope,
  faHeart as farHeart,
} from '@fortawesome/free-regular-svg-icons'

import { wrapIcon } from './FAIcon'

library.add(
  faGithub,
  faLinkedinIn,
  fasGlobe,
  fasTags,
  farEnvelope,
  farHeart,
  fasChevronUp
)

// Brands
export const GithubIcon = wrapIcon(['fab', 'github'])
export const LinkedinIcon = wrapIcon(['fab', 'linkedin-in'])

// Icons
export const GlobeIcon = wrapIcon(['fas', 'globe'])
export const TagsIcon = wrapIcon(['fas', 'tags'])
export const ChevronUpIcon = wrapIcon(['fas', 'chevron-up'])

// Regular (outline) icons
export const EnvelopeOutlineIcon = wrapIcon(['far', 'envelope'])
export const HeartOutlineIcon = wrapIcon(['far', 'heart'])
