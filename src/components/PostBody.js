import React from 'react'
import PropTypes from 'prop-types'
import Content, { HTMLContent } from '../components/Content'

const PostBody = ({ content, isRawHTML }) =>
  isRawHTML ? <HTMLContent content={content} /> : <Content content={content} />

PostBody.propTypes = {
  content: PropTypes.node.isRequired,
  isRawHTML: PropTypes.bool
}

PostBody.defaultProps = {
  isRawHTML: false
}

export default PostBody
