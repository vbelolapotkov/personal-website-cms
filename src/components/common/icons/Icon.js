import React from 'react'

const Icon = ({ children, className, ...props }) => (
  <span className={`icon ${className ? className : ''}`} {...props}>
    {children}
  </span>
)

export default Icon
