import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

const Head = ({ meta }) => (
  <Helmet>
    <html lang="en" />
    <title>{meta.title}</title>
    <meta name="description" content={meta.description} />
  </Helmet>
)

Head.propTypes = {
  meta: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
  }),
}

Head.defaultProps = {
  meta: {
    title: 'Vasily Belolapotkov',
    description: 'Vasily Belolapotkov - Full-Stack developer',
  },
}

export default Head
