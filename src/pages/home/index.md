---
templateKey: home-page
introSection:
  moto: >-
    I use code to express ideas, solve problems and make this world a better
    place to live
  subtitle: Vasily Belolapotkov
  title: Full-stack developer
projectsSection:
  projects:
    - Human-Connection
    - IdeaFox
    - SVG charts
    - Munchkin Web
    - Jackal Dungeon Web
    - Calculator app
  title: Projects
technologySection:
  principles:
    list:
      - description: >
          Software product is alive and always has moving parts like
          requirements, features, bugs, etc. Version control system is a key to
          success in keeping source code under control. I use GitHub for the
          public projects and Bitbucket for the private ones.
        principle: Version control
      - description: >
          Source code indeed has a lot in common with an article or a book. Both
          are expressing ideas with a language. Like an editor who is working
          with a text I perform code refactoring to make it clean, organized and
          easy to read. Clean code saves time and effort on app development,
          maintanance and improvement.
        principle: Clean code
      - description: >
          I belong to the group of developers who believe that an app must have
          tests. Testing ensures the key features are working as expected and
          new features are not breaking anything in the app. That is why I'm
          following a Test Driven Development methodology.
        principle: Testing
    title: Principles
  stack:
    techBlocks:
      - techTags:
          - HTML
          - CSS
          - Less
          - JavaScript
          - ES6
      - techTags:
          - React
          - Redux
          - jQuery
          - Semantic-UI
          - Material-UI
          - Bootstrap
      - techTags:
          - Meteor
          - Node.js
          - Express
          - Gatsby
          - MongoDB
      - techTags:
          - Mocha
          - Jest
      - techTags:
          - AWS
          - Docker
          - Heroku
          - Nginx
    title: Technology stack
  subtitle: >-
    Today developers have a lot of technologies, frameworks, tools and
    methodologies to choose from. Here is my choice of principles to follow and
    tools to create apps.
  title: Technology
contactSection:
  email: belolapotkov.v@gmail.com
  subtitle: Feel free to write me an email
  title: Want to get in touch with me? That's fantastic!
---

