import React from 'react'
import { render } from 'test-utils'

import HomePage from '../home-page'

test('renders home page template with data', () => {
  const introSection = {
    title: 'intro title',
    subtitle: 'intro subtitle',
    moto: 'intro moto'
  }

  const projectsSection = {
    title: 'Projects Section',
    projects: [
      {
        fields: { slug: '/projects/project-1' },
        frontmatter: {
          projectName: 'project 1',
          description: 'decscription 1',
          tech: ['tag1-1', 'tag2-1'],
          thumbnail: { publicUrl: '/img/placeholder-1.jpg' },
          publicUrl: 'http://project-1.local',
          repository: {
            isPrivate: false,
            url: 'http://repo-1.local',
            linkText: 'view src'
          }
        }
      }
    ]
  }

  const technologySection = {
    title: 'Tech section',
    subtitle: 'Tech subtitle',
    principles: {
      title: 'Principles',
      list: [
        {
          principle: 'principle-1',
          description: 'description-1'
        }
      ]
    },
    stack: {
      title: 'Tech stack',
      techBlocks: [
        {
          techTags: ['tag-1', 'tag-2']
        }
      ]
    }
  }

  const contactSection = {
    title: 'Contact',
    subtitle: 'Contact me',
    email: 'me@test.local'
  }

  const data = {
    site: {
      siteMetaData: {
        title: 'title',
        description: 'description'
      }
    },

    markdownRemark: {
      frontmatter: {
        introSection,
        projectsSection,
        technologySection,
        contactSection
      }
    }
  }

  const { container } = render(<HomePage data={data} />)
  expect(container).toMatchSnapshot()
})
