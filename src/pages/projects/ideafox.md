---
templateKey: project-page
projectName: IdeaFox
description: >-
  IdeaFox is a cloud-based platform for teams and organizations to manage ideas
  and innovation processes.
thumbnail: /img/ideafox_thumb.png
tech:
  - Meteor
  - Blaze
  - Semantic-UI
  - AWS
repository:
  isPrivate: true
  linkText: Private source code
publicUrl: 'https://ideafox.io'
---
![IdeaFox project](/img/ideafox_chall.png "IdeaFox project")

IdeaFox is a cloud-based platform for teams and organizations to manage ideas and innovation processes. The platform is developed with flexibility and ease of use in mind to easily adapt to customers needs.

Here is the list of key features:

* Customizable processes with steps (stages) and archives.
* Customizable entry fields with configurable logical dependencies for each process step to collect proper information about an idea along the process.
* Collaboration features (sharing, voting, commenting, following).
* Role-based access management for each project.
* Participants and teams management to allow a group of participants developing ideas along the process.
* Evaluation features including customizable evaluation criteria for each process stage, evaluation team members management and evaluation process controls.
* Milestones for keeping track of progress.
* Exports and reporting features for project managers.

## Tech Stack

* Framework: [Meteor](https://meteor.com)
* UI: [Blaze](https://docs.meteor.com/api/blaze.html), [Semantic UI](https://semantic-ui.com/), other 3rd party components.
* Deployment: Virtual Private Cloud on [AWS](https://aws.amazon.com/)
