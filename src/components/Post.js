import React from 'react'
import PropTypes from 'prop-types'
import { Disqus } from 'gatsby-plugin-disqus'

// TODO: 1. Make tags clickable.
// TODO: 2. Extract post meta into own component.
// TODO: 3. Add post meta. Created at + estimated reading time.
// TODO: 4. Add next/previous post.

const Post = ({ post, children }) => {
  const disqusConfig = {
    url: post.url,
    identifier: post.id,
    title: post.title,
  }
  return (
    <article className="post-container">
      <h1 className="title">{post.title}</h1>
      <div className="meta has-text-grey">
        {post.posted}
        {post.timeToRead && post.timeToRead > 1 && ` | ${post.timeToRead} mins`}
        {post.tags && [' | ', post.tags.join(', ')]}
      </div>
      {children}
      <footer>
        <div className="tags">
          {post.tags &&
            post.tags.length &&
            post.tags.map(tag => (
              <span key={`pt_${tag}`} className="tag">
                {`#${tag}`}
              </span>
            ))}
        </div>
        <Disqus config={disqusConfig} />
      </footer>
    </article>
  )
}

Post.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    posted: PropTypes.string,
    timeToRead: PropTypes.string.number,
    tags: PropTypes.arrayOf(PropTypes.string),
  }),
  children: PropTypes.node,
}

export default Post
