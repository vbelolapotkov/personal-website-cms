---
templateKey: project-page
projectName: VaaS Console
description: VaaS Console is a management app for the cloud video conferencing service.
thumbnail: /img/vaas_console_thumb.jpg
tech:
  - Meteor
  - React
  - Material-UI
repository:
  isPrivate: true
  linkText: Private source code
publicUrl: 'https://console.vaas.croc.ru/'
---
![Vaas Console](/img/vaas_console_thumb.jpg "Vaas Console")

VaaS Console is a management application for a Video-as-a-Service platform. It allows customers to manage owned virtual meeting rooms including meeting access codes which were synchronized with 3rd party video conferencing management servers.

## Tech Stack

* Framework: [Meteor](https://meteor.com).
* UI: [React](https://reactjs.org/), [Redux](https://redux.js.org/), [Material UI](https://material-ui.com/).
* Integrations: [Microsoft Active Directory](https://en.wikipedia.org/wiki/Active_Directory), video conferencing servers.
* Deployment: Private cloud.
