import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import Head from '../components/Head'
import PostBody from '../components/PostBody'
import ProjectMeta from '../components/project/ProjectMeta'

export const ProjectPageTemplate = ({ content, project }) => {
  return (
    <section className="section">
      <div className="container content">
        <div className="columns is-multiline">
          <div className="column is-7 is-offset-4">
            <h1 className="title is-size-2 has-text-weight-bold is-bold-light">
              {project.projectName}
            </h1>
          </div>

          <div className="column is-3">
            <ProjectMeta {...project} />
          </div>

          <div className="column is-6 is-offset-1">{content && content()}</div>
        </div>
      </div>
    </section>
  )
}

ProjectPageTemplate.propTypes = {
  content: PropTypes.func,
  project: PropTypes.shape({
    projectName: PropTypes.string,
    description: PropTypes.string,
    publicUrl: PropTypes.string,
    repository: PropTypes.shape({
      isPrivate: PropTypes.bool,
      url: PropTypes.string,
      linkText: PropTypes.string
    }),
    tech: PropTypes.arrayOf(PropTypes.string)
  })
}

const ProjectPage = ({ data }) => {
  const {
    markdownRemark: { html: content, frontmatter: project }
  } = data

  const meta = {
    title: `${project.projectName} | Project`,
    description: project.description
  }

  return (
    <Layout>
      <Head meta={meta} />
      <ProjectPageTemplate
        content={() => <PostBody content={content} isRawHTML={true} />}
        project={project}
      />
    </Layout>
  )
}

ProjectPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object
  })
}

export default ProjectPage

export const pageQuery = graphql`
  query ProjectByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        projectName
        description
        thumbnail {
          publicURL
          childImageSharp {
            fluid(maxWidth: 600) {
              ...GatsbyImageSharpFluid_tracedSVG
            }
          }
        }
        tech
        repository {
          isPrivate
          url
          linkText
        }
        publicUrl
      }
    }
  }
`
