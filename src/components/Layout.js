import React from 'react'
import PropTypes from 'prop-types'

import Head from './Head'
import Navbar from './Navbar'
import Footer from './Footer'
import ScrollTop from './ScrollTop'

import './all.sass'

if (typeof window !== 'undefined') {
  require('smooth-scroll')('a[href*="#"]')
}

const TemplateWrapper = ({ children }) => (
  <div className="content-container">
    <Head />
    <Navbar />
    <main>{children}</main>
    <Footer />
    <ScrollTop />
  </div>
)

TemplateWrapper.propTypes = {
  meta: Head.propTypes.meta,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
}

export default TemplateWrapper
