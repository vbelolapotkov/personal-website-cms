import React from 'react'
import PropTypes from 'prop-types'

const ContactSection = ({ title, subtitle, email }) => (
  <section className="section is-medium" id="contact">
    <div className="container">
      <div className="content">
        <h2 className="has-text-weight-bold is-size-3">{title}</h2>
        <p className="subtitle">
          <br />
          {subtitle} {email && <a href={`mailto:${email}`}>{email}</a>}
        </p>
      </div>
    </div>
  </section>
)

ContactSection.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  email: PropTypes.string,
}

ContactSection.defaultProps = {
  title: 'Contact',
  subtitle: '',
}

export default ContactSection
