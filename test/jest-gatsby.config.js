/*
* The config is taken from https://www.gatsbyjs.org/docs/unit-testing/
* */

module.exports = {
  transform: {
    '^.+\\.jsx?$': '<rootDir>/test/jest-preprocess.js'
  },

  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy'
    // '.+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js'
  },

  transformIgnorePatterns: ['node_modules/(?!(gatsby)/)'],

  testPathIgnorePatterns: ['node_modules', '.cache'],

  globals: {
    __PATH_PREFIX__: ''
  },

  testURL: 'http://localhost:3000'
}
