import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import {
  IntroSection,
  ProjectsSection,
  TechnologySection,
  ContactSection,
} from '../components/home/sections'

// TODO: 1. Move from CMS page to static page.
// TODO: 2. Add mentroship section.

export const HomePageTemplate = ({
  introSection,
  projectsSection,
  technologySection,
  contactSection,
}) => (
  <div>
    <IntroSection {...introSection} />
    <ProjectsSection {...projectsSection} />
    <TechnologySection {...technologySection} />
    <ContactSection {...contactSection} />
  </div>
)

HomePageTemplate.propTypes = {
  introSection: PropTypes.shape(IntroSection.propTypes),
  projectsSection: PropTypes.shape(ProjectsSection.propTypes),
  technologySection: PropTypes.shape(TechnologySection.propTypes),
  contactSection: PropTypes.shape(ContactSection.propTypes),
}

const HomePage = ({ data }) => {
  const {
    site: { siteMetadata: meta },
  } = data

  const {
    introSection,
    projectsSection,
    technologySection,
    contactSection,
  } = data.markdownRemark.frontmatter

  const projects = projectsSection.projects
    .filter(p => Boolean(p))
    .map(project => ({
      ...project.frontmatter,
      path: project.fields.slug,
    }))

  return (
    <Layout meta={meta}>
      <HomePageTemplate
        introSection={introSection}
        projectsSection={{ ...projectsSection, projects }}
        technologySection={technologySection}
        contactSection={contactSection}
      />
    </Layout>
  )
}

HomePage.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({ siteMetadata: Layout.propTypes.meta }),
    markdownRemark: PropTypes.object,
  }),
}

export default HomePage

export const pageQuery = graphql`
  query HomePageContent($id: String!) {
    site {
      siteMetadata {
        title
        description
      }
    }

    markdownRemark(id: { eq: $id }) {
      frontmatter {
        introSection {
          title
          subtitle
          moto
        }
        projectsSection {
          title
          projects {
            fields {
              slug
            }
            frontmatter {
              projectName
              description
              tech
              thumbnail {
                publicURL
                childImageSharp {
                  fluid(maxWidth: 600) {
                    ...GatsbyImageSharpFluid_tracedSVG
                  }
                }
              }
              publicUrl
              repository {
                isPrivate
                url
                linkText
              }
            }
          }
        }

        technologySection {
          title
          subtitle
          principles {
            title
            list {
              principle
              description
            }
          }
          stack {
            title
            techBlocks {
              techTags
            }
          }
        }

        contactSection {
          title
          subtitle
          email
        }
      }
    }
  }
`
