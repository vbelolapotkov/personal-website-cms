import React from 'react'
import { render } from 'test-utils'
import ProjectMeta from '../ProjectMeta'

const exampleProject = {
  tech: ['JS', 'HTML'],
  repository: {
    isPrivate: false,
    url: 'https://github.com',
    linkText: 'View on github'
  },
  publicUrl: 'http://example.com'
}

test('it should render without error', () => {
  const { container } = render(<ProjectMeta {...exampleProject} />)
  expect(container).toMatchSnapshot()
})

test('it should hide public url if not passed', function() {
  const props = { ...exampleProject }
  props.publicUrl = undefined

  const { queryByText } = render(<ProjectMeta {...props} />)
  expect(queryByText(/view online/i)).not.toBeInTheDocument()
})

test('it should ignore url if repository is private', () => {
  const props = {
    ...exampleProject,
    repository: {
      ...exampleProject.repository,
      isPrivate: true
    }
  }

  const { queryByText } = render(<ProjectMeta {...props} />)

  expect(queryByText(/private source code/i)).toBeInTheDocument()
  expect(queryByText(props.repository.linkText)).not.toBeInTheDocument()
})

test('it should render with default props', () => {
  const { container } = render(<ProjectMeta />)
  expect(container).toMatchSnapshot()
})
