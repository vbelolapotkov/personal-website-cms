import CMS from 'netlify-cms-app'

import HomePagePreview from './preview-templates/HomePagePreview'
import ProjectPagePreview from './preview-templates/ProjectPagePreview'

CMS.registerPreviewTemplate('home', HomePagePreview)
CMS.registerPreviewTemplate('projects', ProjectPagePreview)
