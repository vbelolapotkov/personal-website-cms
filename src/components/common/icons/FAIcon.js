import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Icon from './Icon'

const FAIcon = ({ className, ...props }) => (
  <Icon className={className}>
    <FontAwesomeIcon {...props} />
  </Icon>
)

export const wrapIcon = icon => {
  const component = props => <FAIcon icon={icon} {...props} />
  component.displayName = 'Icon'
  return component
}

export default FAIcon
