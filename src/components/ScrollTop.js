import React, { useCallback, useState, useEffect } from 'react'
import { throttle } from 'lodash'
import { ChevronUpIcon } from './common/icons'

const ScrollTop = () => {
  const scrollToTop = useCallback(
    () => window.scrollTo({ top: 0, behavior: 'smooth' }),
    []
  )
  const [isVisible, setVisibility] = useState(false)
  const updateVisibility = () => {
    const yOffset = window.pageYOffset
    const threshold = window.innerHeight / 2
    setVisibility(yOffset > threshold)
  }
  useEffect(() => {
    updateVisibility()
    const scrollListener = throttle(updateVisibility, 200)
    window.addEventListener('scroll', scrollListener)
    return () => window.removeEventListener('scroll', scrollListener)
  }, [])
  return (
    <button
      className={`button is-link is-outlined scroll-top ${
        isVisible ? '' : 'is-hidden'
      }`}
      onClick={scrollToTop}
    >
      <ChevronUpIcon />
    </button>
  )
}

export default ScrollTop
