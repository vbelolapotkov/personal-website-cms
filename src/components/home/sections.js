export { default as IntroSection } from './IntroSection'
export { default as ProjectsSection } from './ProjectsSection'
export { default as TechnologySection } from './TechnologySection'
export { default as ContactSection } from './ContactSection'

