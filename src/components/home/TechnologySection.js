import React from 'react'
import PropTypes from 'prop-types'

const TechnologySection = ({ title, subtitle, principles, stack }) => (
  <section className="technology section is-medium" id="tech">
    <div className="container">
      <div className="content">
        <h2 className="has-text-weight-bold is-size-2">{title}</h2>
        <p className="subtitle is-size-4">{subtitle}</p>

        <div className="columns">
          <div className="column is-7-tablet">
            <h3 className="is-size-3">{principles.title}</h3>
            {principles.list &&
              typeof principles.list.map === 'function' &&
              principles.list.map(({ principle, description }) => (
                <section key={`principle_${principle}`} className="content">
                  <h4>{principle}</h4>
                  <p>{description}</p>
                </section>
              ))}
          </div>
          <div className="column is-4-tablet is-offset-1-tablet">
            <h4 className="is-size-3">{stack.title}</h4>
            <div className="content">
              {stack.techBlocks &&
                typeof stack.techBlocks.map === 'function' &&
                stack.techBlocks.map(({ techTags }, blockIndex) => (
                  <div className="tags" key={`techBlock-${blockIndex}`}>
                    {techTags.map((tag, tagIndex) => (
                      <div
                        key={`tag-${blockIndex}-${tagIndex}`}
                        className="tag is-white gtm-tech-tag"
                      >
                        {tag}
                      </div>
                    ))}
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

TechnologySection.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  principles: PropTypes.shape({
    title: PropTypes.string,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        principle: PropTypes.string,
        description: PropTypes.string,
      })
    ),
  }),
  stack: PropTypes.shape({
    title: PropTypes.string,
    techBlocks: PropTypes.arrayOf(
      PropTypes.shape({
        techTags: PropTypes.arrayOf(PropTypes.string),
      })
    ),
  }),
}

TechnologySection.defaultProps = {
  title: 'Technology',
  subtitle: '...',
  principles: {
    title: 'Principles',
    list: [],
  },
  stack: {
    title: 'Technology stack',
    techBlocks: [],
  },
}

export default TechnologySection
