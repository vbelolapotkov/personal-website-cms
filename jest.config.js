const path = require('path')

const jestGatsbyConfig = require('./test/jest-gatsby.config')
module.exports = {
  ...jestGatsbyConfig,
  testEnvironment: 'jest-environment-jsdom',
  moduleDirectories: ['node_modules', path.join(__dirname, 'test')],
  moduleNameMapper: {
    ...jestGatsbyConfig.moduleNameMapper,
    '^@(utils|components)(.*)$': '<rootDir>/src/$1$2',
  },
  watchPathIgnorePatterns: ['node_modules', '.cache', 'public', 'static'],
  setupFilesAfterEnv: ['./test/setup-tests.js'],
}
