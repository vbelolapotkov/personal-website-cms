import React from 'react'
import PropTypes from 'prop-types'

import { GithubIcon, GlobeIcon, TagsIcon } from '../common/icons'
import { ExternalLink } from '../common'

const ProjectMeta = ({ publicUrl, repository, tech }) => (
  <div className="panel">
    <div className="panel-heading">Project info</div>
    {publicUrl && (
      <div className="panel-block">
        <GlobeIcon className="panel-icon" />
        <div className="content has-text-centered">
          <ExternalLink href={publicUrl}>View online</ExternalLink>
        </div>
      </div>
    )}
    <div className="panel-block">
      <GithubIcon className="panel-icon" />
      <div className="content">
        {repository.isPrivate ? (
          <span>Private source code</span>
        ) : (
          <ExternalLink href={repository.url}>
            {repository.linkText}
          </ExternalLink>
        )}
      </div>
    </div>
    <div className="panel-block">
      <TagsIcon className="panel-icon" />
      <ul className="taglist">
        {tech.map(tag => (
          <li key={`${tag}_tag`} className="gtm-tech-tag">
            {tag}
          </li>
        ))}
      </ul>
    </div>
  </div>
)

ProjectMeta.propTypes = {
  publicUrl: PropTypes.string,
  repository: PropTypes.shape({
    isPrivate: PropTypes.bool,
    url: PropTypes.string,
    linkText: PropTypes.string
  }),
  tech: PropTypes.arrayOf(PropTypes.string)
}

ProjectMeta.defaultProps = {
  publicUrl: '',
  repository: { isPrivate: true },
  tech: []
}

export default ProjectMeta
