import React from 'react'
import { render } from '@testing-library/react'
import { axe } from 'jest-axe'
import ProjectCard from '../ProjectCard'

describe('ProjectCard', () => {
  const project = {
    projectName: 'Test project',
    path: '/project/test-project',
    thumbnail: { publicUrl: '/img/fake.png' },
    publicUrl: 'http://project.local',
    description: 'Test description',
    tech: ['js', 'react'],
    repository: {
      isPrivate: false,
      linkText: 'View on GitHub',
      url: 'http://repo.local'
    }
  }

  test('should render the card with public repository', async () => {
    const { container, getByText, queryByText } = render(
      <ProjectCard project={project} />
    )

    const title = getByText(project.projectName)
    expect(title).toHaveAttribute('href', project.path)

    expect(queryByText(project.description)).toBeInTheDocument()

    expect(container.querySelector('a.card-image')).toHaveAttribute(
      'href',
      project.path
    )

    const repoLink = getByText(project.repository.linkText)
    expect(repoLink).toHaveAttribute('href', project.repository.url)

    const checkTag = tag => {
      const tagElement = getByText(tag)
      expect(tagElement).toHaveClass('is-info')
    }

    project.tech.forEach(checkTag)

    const a11yReport = await axe(container.innerHTML)
    expect(a11yReport).toHaveNoViolations()
  })

  test('should render the card with private repo', () => {
    const privateProject = {
      ...project,
      repository: {
        isPrivate: true,
        linkText: 'Private src code'
      }
    }
    const { getByText } = render(<ProjectCard project={privateProject} />)
    const repoLink = getByText(privateProject.repository.linkText)
    expect(repoLink.tagName).toBe('SPAN')
  })

  test('should render gatsby-image', () => {
    const childImageSharp = {
      fluid: {
        tracedSVG: 'data:image/svg+xml...',
        aspectRatio: 1.3725490196078431,
        src: '/static/thumb.png',
        srcSet:
          '/static/thumb.png 150w,\n/static/300/thumb.png 300w,\n/static/600/thumb.png 600w,\n/static/700/thumb.png 700w',
        sizes: '(max-width: 600px) 100vw, 600px'
      }
    }
    const projectWithThumb = {
      ...project,
      thumbnail: {
        childImageSharp
      }
    }

    const { container } = render(<ProjectCard project={projectWithThumb} />)
    expect(container).toMatchSnapshot()
  })
})
