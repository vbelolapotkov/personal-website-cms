import React from 'react'
import { render } from 'test-utils'

import { TechnologySection } from '../sections'

test('renders without props', () => {
  const { container } = render(<TechnologySection />)
  expect(container).toMatchSnapshot()
})

test('renders with empty tech stack', () => {
  const emptyStack = {}
  const { container } = render(<TechnologySection stack={emptyStack} />)
  expect(container).toMatchSnapshot()
})

test('renders with empty principles list', () => {
  const emptyPrinciples = {
    title: 'Principles'
  }
  const { container } = render(
    <TechnologySection principles={emptyPrinciples} />
  )
  expect(container).toMatchSnapshot()
})
