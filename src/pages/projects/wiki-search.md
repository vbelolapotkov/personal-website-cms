---
templateKey: project-page
projectName: Wiki search
description: Wiki Search is a simple app for searching articles on wikipedia.org.
thumbnail: /img/wiki_search_thumb.jpg
tech:
  - jQuery
  - Bootstrap
repository:
  linkText: View on Codepen
  url: 'http://codepen.io/vbelolapotkov/pen/OXXVVO'
publicUrl: 'http://codepen.io/vbelolapotkov/full/OXXVVO/'
---
![Wiki search demo](/img/wiki_search_demo.gif "Wiki search demo")

This is a simple search widget for [Wikipedia](https://www.wikipedia.org/) articles. The project was implemented as part of [freeCodeCamp](https://www.freecodecamp.org/) curriculum. It queries Wikipedia API for search results and random articles based on a request from the search field. The widget’s functionality is implemented with [jQuery](https://jquery.com/). [Bootstrap](https://getbootstrap.com/) is used for styling. The widget is developed and deployed on a [CodePen](https://codepen.io/).
